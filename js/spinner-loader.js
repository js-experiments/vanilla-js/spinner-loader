const loader = document.querySelector('.loader');

let rotation = 0;

function removeLine(e) {
  this.parentNode.remove();
}

function addLine() {
  const container = document.createElement('div');
  container.classList.add('line-container');
  container.style.transform = `rotate(${rotation}deg)`;
  rotation += 15;

  const line = document.createElement('div');
  line.classList.add('line');
  container.appendChild(line)
  loader.appendChild(container);

  line.addEventListener('animationend', removeLine);
}

const intervalId = setInterval(addLine, 30);
